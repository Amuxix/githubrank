# GithubRank challenge

This repository presents a solution to the code challenge proposed by scalac for the recruitment process.

It aims to implement a simple server that exposes a single endpoint that returns a list sorted by the number of
contributions made by developer to all repositories for the given organization.

### How to run

The simplest way to run this is to use [sbt native packager](https://sbt-native-packager.readthedocs.io/).
As an example for running this in windows you run `sbt universal:packageBin` and it will generate a zip file(native packager
supports a bunch of other formats that are described in their website).
With the zip created we extract it and run `set GH_TOKEN=<your_github_token_here>` to set the environment variable for the token.
With that done all that remains is to run the `.bat` file inside the bin folder that we extracted from the zip.
The file should be called `githubrank.bat`.

### Things that could be improved

The cache is never invalidated which is useless in a prod environment.
The error when max retries is reached is not very clear.
