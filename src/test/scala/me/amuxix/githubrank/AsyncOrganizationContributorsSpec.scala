package me.amuxix.githubrank

import cats.effect.scalatest.{AssertingSyntax, EffectTestSupport}
import cats.effect.{ContextShift, IO, Timer}
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock._
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.options
import cats.implicits.catsKernelStdMonoidForList
import io.circe.generic.auto._
import io.circe.syntax._
import org.http4s._
import org.http4s.circe.CirceEntityDecoder.circeEntityDecoder
import org.http4s.client.blaze.BlazeClientBuilder
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AsyncWordSpec
import org.scalatest.{Assertions, BeforeAndAfterAll, OptionValues}
import me.amuxix.githubrank.GithubRank.ClientOps
import me.amuxix.githubrank.github.{Contribution, Repository}
import me.amuxix.githubrank.routes.OrganizationContributors
import me.amuxix.githubrank.OrganizationNotFound
import me.amuxix.githubrank.dto.Contributor
import org.http4s.client.Client

import scala.concurrent.ExecutionContext
import scala.concurrent.ExecutionContext.Implicits.global

class AsyncOrganizationContributorsSpec
    extends AsyncWordSpec
    with AssertingSyntax
    with EffectTestSupport
    with Assertions
    with OptionValues
    with Matchers
    with BeforeAndAfterAll {
  private val port = 8081
  private val hostname = "localhost"

  implicit private lazy val settings = Settings(
    token = "as5fda5fads54gfagds545afd",
    githubApiHost = Uri.unsafeFromString(s"http://$hostname:$port"),
  )

  override val executionContext: ExecutionContext = ExecutionContext.global
  implicit val ioContextShift: ContextShift[IO] = IO.contextShift(executionContext)
  implicit val ioTimer: Timer[IO] = IO.timer(executionContext)

  // Run wiremock server on local machine with specified port.
  private val wireMockServer = new WireMockServer(options().port(port))
  private val (c, finalizer) = BlazeClientBuilder[IO](global).resource.allocated.unsafeRunSync()
  implicit private val client: Client[IO] = c
  private val organizationContributors = new OrganizationContributors

  override protected def beforeAll(): Unit =
    wireMockServer.start()

  override protected def afterAll(): Unit = {
    wireMockServer.stop()
    finalizer.unsafeRunSync()
  }

  "streamAllPages" should {
    "give a stream of all elements of each page starting from the given URI" in {
      val page1Path = "/test"
      val page2Path = "/test/1"
      val page1Response = List(1, 2)
      val page2Response = List(3, 4)
      wireMockServer.stubFor(
        get(urlPathEqualTo(page1Path)).willReturn(
          aResponse()
            .withHeader("Content-Type", "application/json")
            .withHeader("Link", s"""<${settings.githubApiHost withPath page2Path}>; rel=\"next\"""")
            .withBody(page1Response.asJson.toString)
            .withStatus(200)
        )
      )

      wireMockServer.stubFor(
        get(urlPathEqualTo(page2Path)).willReturn(
          aResponse()
            .withHeader("Content-Type", "application/json")
            .withHeader("Link", s"""<${settings.githubApiHost withPath page1Path}>; rel=\"prev\"""")
            .withBody(page2Response.asJson.toString)
            .withStatus(200)
        )
      )

      client
        .streamAllPages(settings.githubApiHost withPath page1Path, Headers.empty)(GithubRank.handleSuccess[IO, Int])
        .compile
        .foldMonoid
        .asserting(_ shouldBe page1Response ++ page2Response)
    }
  }

  "getContributions" should {
    val organizationName = "test"
    val organizationPath = s"/orgs/$organizationName/repos"
    "get a list of contributions for the given organization name" in {
      val repositoryOwner = github.Entity(organizationName)
      val repo1 = Repository("repoTest1", repositoryOwner)
      val repo2 = Repository("repoTest2", repositoryOwner)
      val repositories = List(
        repo1,
        repo2,
      )

      wireMockServer.stubFor(
        get(urlPathEqualTo(organizationPath)).willReturn(
          aResponse()
            .withHeader("Content-Type", "application/json")
            .withBody(repositories.asJson.toString)
            .withStatus(200)
        )
      )

      val amuxix = github.Entity("Amuxix")
      val danielle = github.Entity("Danielle")
      val repo1Contributors = List(
        Contribution(5, amuxix),
        Contribution(3, danielle),
      )

      val repo2Contributors = List(
        Contribution(3875, danielle),
        Contribution(552, amuxix),
      )

      val repo1PathContributors = s"/repos/${repo1.owner.login}/${repo1.name}/stats/contributors"
      val repo2PathContributors = s"/repos/${repo2.owner.login}/${repo2.name}/stats/contributors"

      wireMockServer.stubFor(
        get(urlPathEqualTo(repo1PathContributors)).willReturn(
          aResponse()
            .withHeader("Content-Type", "application/json")
            .withBody(repo1Contributors.asJson.toString)
            .withStatus(200)
        )
      )

      wireMockServer.stubFor(
        get(urlPathEqualTo(repo2PathContributors)).willReturn(
          aResponse()
            .withHeader("Content-Type", "application/json")
            .withBody(repo2Contributors.asJson.toString)
            .withStatus(200)
        )
      )

      organizationContributors
        .getContributions(organizationName)
        .asserting(_ should contain theSameElementsAs (repo1Contributors ++ repo2Contributors))
    }

    "give OrganizationNotFound if organization name is invalid" in {
      wireMockServer.stubFor(
        get(urlPathEqualTo(organizationPath)).willReturn(
          aResponse()
            .withStatus(204)
            .withHeader("Content-Type", "application/json")
            .withBody(List(1, 2).asJson.toString())
        )
      )
      organizationContributors.getContributions("invalid-name").assertThrows[OrganizationNotFound.type]
    }
  }

  "organizationContributors" should {
    val organizationName = "test"
    val organizationPath = s"/orgs/$organizationName/repos"
    val repositoryOwner = github.Entity(organizationName)
    val repo1 = Repository("repoTest1", repositoryOwner)
    val repo2 = Repository("repoTest2", repositoryOwner)
    val repositories = List(
      repo1,
      repo2,
    )

    val amuxix = github.Entity("Amuxix")
    val danielle = github.Entity("Danielle")
    val repo1Contributors = List(
      Contribution(5, amuxix),
      Contribution(3, danielle),
    )

    val repo2Contributors = List(
      Contribution(3875, danielle),
      Contribution(552, amuxix),
    )

    val repo1PathContributors = s"/repos/${repo1.owner.login}/${repo1.name}/stats/contributors"
    val repo2PathContributors = s"/repos/${repo2.owner.login}/${repo2.name}/stats/contributors"
    "return a list of contributions with all contributions from an author summed together and sorted" in {

      wireMockServer.stubFor(
        get(urlPathEqualTo(organizationPath)).willReturn(
          aResponse()
            .withHeader("Content-Type", "application/json")
            .withBody(repositories.asJson.toString)
            .withStatus(200)
        )
      )
      wireMockServer.stubFor(
        get(urlPathEqualTo(repo1PathContributors)).willReturn(
          aResponse()
            .withHeader("Content-Type", "application/json")
            .withBody(repo1Contributors.asJson.toString)
            .withStatus(200)
        )
      )

      wireMockServer.stubFor(
        get(urlPathEqualTo(repo2PathContributors)).willReturn(
          aResponse()
            .withHeader("Content-Type", "application/json")
            .withBody(repo2Contributors.asJson.toString)
            .withStatus(200)
        )
      )

      organizationContributors
        .organizationContributors(organizationName)
        .asserting(_ should contain theSameElementsInOrderAs (List(
          Contributor(danielle.login, 3878),
          Contributor(amuxix.login, 557),
        )))
    }

    "give OrganizationNotFound if organization name is invalid" in {
      wireMockServer.stubFor(
        get(urlPathEqualTo(organizationPath)).willReturn(
          aResponse()
            .withStatus(204)
            .withHeader("Content-Type", "application/json")
            .withBody(List(1, 2).asJson.toString())
        )
      )
      organizationContributors.getContributions("invalid-name").assertThrows[OrganizationNotFound.type]
    }
  }
}
