package me.amuxix.githubrank

import cats.effect.IO
import cats.effect.scalatest.{AssertingSyntax, EffectTestSupport}
import me.amuxix.githubrank.routes.OrganizationContributors
import org.http4s._
import org.http4s.circe.CirceEntityDecoder.circeEntityDecoder
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.{Assertions, OptionValues}

class OrganizationContributorsSpec
    extends AnyWordSpec
    with AssertingSyntax
    with EffectTestSupport
    with Assertions
    with OptionValues
    with Matchers {
  def failure[T](t: T): Nothing = fail()


  "parseContributionsResponse" should {
    "return an empty list if response was NoContent" in {
      val response = Response[IO](Status.NoContent)
      OrganizationContributors.parseContributionsResponse[IO, Int].applyOrElse(response, failure).asserting {
        case (parsedBody, _) => parsedBody shouldBe empty
      }
    }
  }

  "parseRepositoriesResponse" should {
    "give OrganizationNotFound exception if response was NotFound" in {
      val response = Response[IO](Status.NotFound)
      OrganizationContributors
        .parseRepositoriesResponse[IO, Int]
        .applyOrElse(response, failure)
        .assertThrows[OrganizationNotFound.type]
    }
  }
}
