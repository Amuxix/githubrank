package me.amuxix.githubrank

import cats.effect.IO
import cats.effect.scalatest.{AssertingSyntax, EffectTestSupport}
import org.http4s._
import org.http4s.circe.CirceEntityDecoder.circeEntityDecoder
import org.http4s.circe.CirceEntityEncoder.circeEntityEncoder
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.{Assertions, OptionValues}

class GithubRankSpec
    extends AnyWordSpec
    with AssertingSyntax
    with EffectTestSupport
    with Assertions
    with OptionValues
    with Matchers  {
  def failure[T](t: T): Nothing = fail()

  "nextPageURL" should {

    "extract an URI for next page if it exists" in {
      val uriString = "localhost"
      val link = Header("Link", s"""<$uriString>; rel=\"next\"""")
      val response: Response[IO] = Response[IO](
        Status.Ok,
        headers = Headers.of(link)
      )
      val nextPageURL = GithubRank.nextPageURL(response)
      nextPageURL.value shouldEqual Uri.unsafeFromString(uriString)
    }

    "return None if next page does not exist" in {
      val response: Response[IO] = Response[IO](Status.Ok)
      GithubRank.nextPageURL(response) shouldBe empty
    }
  }
  "handleSuccess" should {
    "transform the response into a List[T] if response is successful" in {
      val body = List(1, 2, 3)
      val response = Response[IO](Status.Ok).withEntity(body)
      GithubRank.handleSuccess[IO, Int].applyOrElse(response, failure).asserting {
        case (parsedBody, _) => parsedBody shouldEqual body
      }
    }
    "not be defined if response is not successful" in {
      val response = Response[IO](Status.NotFound)
      GithubRank.handleSuccess[IO, Int].isDefinedAt(response) shouldBe false
    }
  }
}
