package me.amuxix.githubrank.routes

import cats.effect.{Concurrent, Sync, Timer}
import cats.implicits.{catsKernelStdMonoidForList, toFunctorOps}
import fs2.Stream
import io.circe.generic.auto._
import me.amuxix.githubrank.GithubRank.ClientOps
import me.amuxix.githubrank.dto.Contributor
import me.amuxix.githubrank.github.{Contribution, Entity, Repository}
import me.amuxix.githubrank.{GithubRank, OrganizationNotFound, Settings}
import org.http4s.Status.{Accepted, NoContent, NotFound}
import org.http4s.circe.CirceEntityDecoder.circeEntityDecoder
import org.http4s.client.Client
import org.http4s.client.middleware.{Retry, RetryPolicy}
import org.http4s.headers.Authorization
import org.http4s.{Entity => _, _}

import scala.concurrent.duration.DurationInt

class OrganizationContributors[F[_]: Timer: Concurrent](implicit client: Client[F], settings: Settings) {
  /**
   * Gets a lists of contributions for each repository in a given organization
   * @param organizationName Name of the organization we want to get contributions for
   * @return A list of contributions of an organization
   */
  def getContributions(organizationName: String): F[List[Contribution]] = {
    val retryPolicy = RetryPolicy[F](
      RetryPolicy.exponentialBackoff(settings.maxWaitSeconds.seconds, settings.maxRetries),
      (request, response) => RetryPolicy.defaultRetriable(request, response) || response.exists(_.status == Accepted)
    )

    val clientWithRetry = Retry(retryPolicy)(client)

    val headers = Headers.of(
      Authorization(Credentials.Token(AuthScheme.Bearer, settings.token)),
    )

    val repositoriesURL = settings.githubApiHost / "orgs" / organizationName / "repos"

    (for {
      repositories <- client.streamAllPages[Repository](repositoriesURL, headers)(OrganizationContributors.parseRepositoriesResponse)
      contributions <- Stream
        .emits(repositories)
        .map { repository =>
          val contributionsURL =
            settings.githubApiHost / "repos"/ repository.owner.login / repository.name / "stats" / "contributors"
          println(contributionsURL)
          clientWithRetry.streamAllPages[Contribution](contributionsURL, headers)(OrganizationContributors.parseContributionsResponse)
        }
        .parJoin(settings.parallelism)
    } yield contributions).compile.foldMonoid
  }

  /**
   * Compiles a list, sorted by total contributions amount, largest to smallest, of total contributions for an organization
   * @param organizationName Name of the organization we want to get contributions for
   * @return A list of contributions sorted by amount
   */
  def organizationContributors(organizationName: String): F[List[Contributor]] =
    for {
      contributions <- getContributions(organizationName)
      sortedContributions = contributions.groupBy(_.author)
        .toList
        .map {
          case (Entity(login), contributions) => Contributor(login, contributions.foldLeft(0)(_ + _.total))
        }
        .sortBy(_.contributions)(Ordering.Int.reverse)
      _ = println(sortedContributions)
    } yield sortedContributions
}

object OrganizationContributors {
  /**
   * Parses the responses from the repository stats endpoint
   */
  def parseContributionsResponse[F[_], T](
    implicit F: Sync[F],
    decoder: EntityDecoder[F, List[T]]
  ): PartialFunction[Response[F], F[(List[T], Option[Uri])]] =
    new PartialFunction[Response[F], F[(List[T], Option[Uri])]] {
      override def isDefinedAt(response: Response[F]): Boolean = response.status == NoContent
      override def apply(response: Response[F]): F[(List[T], Option[Uri])] = F.pure((List.empty, None))
    }
      .orElse(GithubRank.handleSuccess)

  /**
   * Parses the responses from the repository list endpoint
   */
  def parseRepositoriesResponse[F[_], T](
    implicit F: Sync[F],
    decoder: EntityDecoder[F, List[T]]
  ): PartialFunction[Response[F], F[(List[T], Option[Uri])]] = GithubRank.handleSuccess.orElse {
    case failed if failed.status == NotFound => F.raiseError(OrganizationNotFound)
  }
}
