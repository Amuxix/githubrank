package me.amuxix.githubrank

import org.http4s.Uri
import org.http4s.syntax.literals._
import com.typesafe.config.{Config, ConfigFactory}
import pureconfig.ConfigConvert.viaNonEmptyStringTry
import pureconfig.{ConfigConvert, ConfigSource}
import pureconfig.generic.auto._

case class Settings (
  token: String,
  serverPort: Int = 8080,
  githubApiHost: Uri = uri"https://api.github.com",
  responseTimeoutSeconds: Int = 60,
  maxIdleTimeoutSeconds: Int = 60,
  maxRetries: Int = 10,
  maxWaitSeconds: Int = 30,
  parallelism: Int = Runtime.getRuntime.availableProcessors,
) {
  require(token.length > 0, "Token must not be empty")
}

object Settings {
  implicit val http4sUriConfigConvert: ConfigConvert[Uri] = viaNonEmptyStringTry[Uri](Uri.fromString(_).toTry, _.toString)
   def fromConfig(config: Config = ConfigFactory.load()): Settings = ConfigSource.fromConfig(config).at("githubRank").loadOrThrow[Settings]
}