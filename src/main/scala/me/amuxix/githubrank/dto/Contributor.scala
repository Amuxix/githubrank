package me.amuxix.githubrank.dto

case class Contributor(
  name: String,
  contributions: Int
)
