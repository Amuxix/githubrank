package me.amuxix.githubrank

import cats.effect.{ExitCode, IO, IOApp, Sync, Timer}
import cats.implicits.{catsStdInstancesForOption, catsSyntaxApplicativeError, toFunctorOps, toTraverseOps}
import fs2.Stream
import io.circe.generic.auto._
import io.circe.syntax._
import me.amuxix.githubrank.routes.OrganizationContributors
import org.http4s.Status.Successful
import org.http4s.circe.CirceEntityEncoder.circeEntityEncoder
import org.http4s.client.Client
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.dsl.io._
import org.http4s.implicits._
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.{Entity => _, _}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt

object GithubRank extends IOApp {

  /**
   * Attempts to extract the URL for the next page from a response.
   * @param response Response to extract the URL from
   * @return An option of URL, None when no next page URL could be found
   */
  def nextPageURL[F[_]](response: Response[F]): Option[Uri] = {
    val link = response.headers.find(_.name.toString == "Link")
    link.flatMap { header =>
      //Regex that will extract pagination links
      val pattern = "<(.+)>; rel=\"(.+)\"".r
      header.value.split(", ").collectFirst {
        //Only match link for next page
        case pattern(link, "next") => Uri.fromString(link).toOption
      }
        .flatten
    }
  }

  /**
   * Transforms the response into the desired type when it's a successful response
   */
  def handleSuccess[F[_]: Sync, T](
    implicit decoder: EntityDecoder[F, List[T]]
  ): PartialFunction[Response[F], F[(List[T], Option[Uri])]] = {
    case Successful(response) => response.as[List[T]].map((_, nextPageURL(response)))
  }

  implicit class ClientOps[F[_]](client: Client[F])(implicit F: Sync[F], timer: Timer[F]) {

    /**
     * Grabs all the pages from a given URL and places them in a Stream
     * @param url The url for the first page
     * @param headers Headers to include in the requests
     * @param parseResponse A Partial Function to parse all the successful responses
     * @return A Stream of pages each with a list of the elements of that page
     */
    def streamAllPages[T](
      url: Uri,
      headers: Headers
    )(
      parseResponse: PartialFunction[Response[F], F[(List[T], Option[Uri])]]
    ): Stream[F, List[T]] =
      Stream.unfoldEval(Option(url))(_.traverse { nextPageUrl =>
        val request = Request[F](
          Method.GET,
          nextPageUrl,
          headers = headers
        )

        def fullParseResponse(response: Response[F]): F[(List[T], Option[Uri])] = response match {
          case response if parseResponse.isDefinedAt(response) => parseResponse(response)
          case failed => F.raiseError(UnexpectedStatus(failed.status))
        }

        client.fetch(request)(fullParseResponse)
      })
  }

  override def run(args: List[String]): IO[ExitCode] = {
    implicit val settings: Settings = Settings.fromConfig()

    def githubRankService(implicit client: Client[IO]) = HttpRoutes.of[IO] {
      case GET -> Root / "org" / organizationName / "contributors" =>
        val contributors = new OrganizationContributors()
          .organizationContributors(organizationName)
          .map(_.asJson)
            .recover {
            case OrganizationNotFound               => s"Organization '$organizationName' not found".asJson
            case unexpectedStatus: UnexpectedStatus => unexpectedStatus.getMessage.asJson
          }
        Ok(contributors)
      }
      .orNotFound

    (for {
      client <- BlazeClientBuilder[IO](global).stream
      exitCode <- BlazeServerBuilder[IO]
        .withResponseHeaderTimeout(settings.responseTimeoutSeconds.seconds)
        .withIdleTimeout(settings.maxIdleTimeoutSeconds.seconds)
        .bindLocal(settings.serverPort)
        .withHttpApp(HttpCaching(githubRankService(client)))
        .serve
    } yield exitCode).compile.drain.as(ExitCode.Success)
  }
}
