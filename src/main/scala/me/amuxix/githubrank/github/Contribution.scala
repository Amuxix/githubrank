package me.amuxix.githubrank.github

case class Contribution(total: Int, author: Entity)
