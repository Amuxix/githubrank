package me.amuxix.githubrank.github

/**
 * This class represents either a user or an organization.
 *
 * @param login Login name of the entity
 */
case class Entity(login: String)
