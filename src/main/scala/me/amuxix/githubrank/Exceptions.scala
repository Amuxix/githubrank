package me.amuxix.githubrank

import org.http4s.Status

import scala.util.control.NoStackTrace

sealed trait GithubRankException extends RuntimeException with NoStackTrace
case class UnexpectedStatus(status: Status) extends GithubRankException {
  override def getMessage: String = s"unexpected HTTP status: $status"
}
case object OrganizationNotFound extends GithubRankException